# Triage API

This is a RESTful API for accessing the data from [Sushrut](https://
sushrut.codes)

## Chat
If you have suggestions that you would like to share then join either [Element](https://matrix.to/#/!xfABEliXqOZqiOpYOs:matrix.org?via=matrix.org&via=t2bot.io&via=gitter.im) or [Discord](https://discord.gg/SK5sPPdf). Messages are relayed from one to the other. We are live on Element.