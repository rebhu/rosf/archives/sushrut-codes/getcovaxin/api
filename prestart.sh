#!/usr/bin/env bash

sed -i '17s/.*/from werkzeug.utils import cached_property/' /usr/local/lib/python3.6/site-packages/flask_restplus/fields.py
sed -i '24s/.*/from werkzeug.utils import cached_property/' /usr/local/lib/python3.6/site-packages/flask_restplus/api.py

# python3.6 manage.py db init
# python3.6 manage.py db revision --rev-id $REV_ID
# python3.6 manage.py db migrate
# python3.6 manage.py db upgrade
