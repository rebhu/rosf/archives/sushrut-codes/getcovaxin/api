# script defining error responses

def error_response_400(message):
    """Bad request: missing data"""
    return {'message': message}, 400

def error_response_401(message):
    """Unauthorized: Invalid token"""
    return {'message': message}, 401

def error_response_403(message):
    """Resource error: unable to process request"""
    return {'message': message}, 403

def error_response_406(message):
    """Validation error"""
    return {'message': message}, 406

def error_response_415(message):
    """Unsupported media type"""
    return {'message': message}, 415

def error_response_500(message):
    """Internal server error"""
    return {'message': message}, 500