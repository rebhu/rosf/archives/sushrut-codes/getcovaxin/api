import re

from flask import current_app as app
from app.main import db
from ..model.getcovaxin_model import Pincodes

def get_pincode_id(pincode):
    try:
        pincode_id = Pincodes.query.filter_by(pincode=pincode).with_entities(db.func.BIN_TO_UUID(Pincodes.pincode_id)).first()
        if not pincode_id:
            return False, "No pincode found"

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message

    return True, pincode_id[0]