from app.main.model.getcovaxin_model import Pincodes, Requests, SlotRequests
import requests
import json
import datetime as dt
import time
import os
import random

from flask import current_app as app
from pywebpush import webpush, WebPushException
from app.main import db

# ----------------------------------------------------
# LOGGING
# ----------------------------------------------------
def log_push_web_error(log_info):
    log_file = log_info['file_name']
    dateTime = dt.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    log_file.write('%19s   batch=%03d   pincode=%6d   request_id=%36s   error=%s\n' %(dateTime, log_info['batch'], log_info['pincode'], log_info['request_id'], log_info['message']))
    



# def get_pincode_slots(pincode):
    
    # url = app.config['COWIN_URL']

    # headers = {
    #         'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0',
    #         'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    #         'Accept-Encoding': 'gzip, deflate, br',
    #         'Accept-Language': 'en-US,en;q=0.5',
    #         'Cache-Control': 'no-cache',
    #         'Connection': 'keep-alive',
    #         'Host': 'cdn-api.co-vin.in' ,
    #         'Pragma': 'no-cache',
    #         'DNT': '1',
    #         'Upgrade-Insecure-Requests': '1'
    #         }
    # dt_format = "%d-%m-%Y"
    # dt_now = dt.datetime.now()
    # dt_now_IST = (dt_now + dt.timedelta(hours=5, minutes=30)).strftime(dt_format)

    # params = {
    #         'pincode': pincode,
    #         'date': dt_now_IST
    #         }

    # resp = requests.get(url, headers=headers, params=params)
    
    # if resp.status_code != 200:
    #     return False, {}, {}

    # data = json.loads(resp.text)
    # slots18, dateobj18, cids18 = [], [], []
    # slots45, dateobj45, cids45 = [], [], []

    # for center in data['centers']:
    #     cid = center['center_id']
    #     for session in center['sessions']:
    #         if session['min_age_limit'] == 18:
    #             cids18.append(cid)
    #             slots18.append(session['available_capacity'])
    #             dtobj = dt.datetime.strptime(session['date'], dt_format).date()
    #             dateobj18.append(dtobj)

    #         else:
    #             cids45.append(cid)
    #             slots45.append(session['available_capacity'])
    #             dtobj = dt.datetime.strptime(session['date'], dt_format).date()
    #             dateobj45.append(dtobj)
    
    # if slots18:
    #     total_slots18 = sum(slots18)
    #     daysobj = max(dateobj18) - min(dateobj18)
    #     days18 = daysobj.days
    #     center18 = len(set(cids18))
    # else:
    #     total_slots18, days18, center18 = 0,0,0

    # if slots45:
    #     total_slots45 = total_slots18 + sum(slots45)
    #     daysobj = max(dateobj45) - min(dateobj45)
    #     days45 = max(days18, daysobj.days)
    #     center45 = center18 +  len(set(cids45))
    # else:
    #     total_slots45, days45, center45 = 0,0,0

    # plus_18 = {
    #         'slots': total_slots18,
    #         'days': days18,
    #         'center': center18
    #         }
    # plus_45 = {
    #         'slots': total_slots45,
    #         'days': days45,
    #         'center': center45
    #         }
    
    # plus_18 = {
    #         "slots": 18,
    #         "days": 3,
    #         "center": 2
    #         }
    # plus_45 = {
    #         "slots": 45,
    #         "days": 3,
    #         "center": 4
    #         }
    # return True, plus_18, plus_45

# def get_pincodes_and_status(batch_no):
#     try:
#         pincode_resp = Pincodes.query.filter_by(batch_no=batch_no).with_entities(Pincodes.pincode, db.func.BIN_TO_UUID(Pincodes.pincode_id)).all()
        
#         if not pincode_resp:
#             return False, 'No pincodes yet'

#     except Exception as err:
#         message = re.findall(r'\((.*?)\)', err.args[0])[-1]

#         return False, message
#     pincodes = [p[0] for p in pincode_resp]
#     pincode_ids = [p[1] for p in pincode_resp]
#     return True, pincodes, pincode_ids

# def get_slots(batch_no):
#     try:
#         slots_resp = Pincodes.query.filter_by(batch_no=batch_no).with_entities(Pincodes.pincode, Pincodes.plus18_status, Pincodes.plus45_status, Pincodes.plus18, Pincodes.plus45).all()
#         if not slots_resp:
#             return False, 'No pincodes yet'

#     except Exception as err:
#         message = re.findall(r'\((.*?)\)', err.args[0])[-1]
    
#     pincodes = [p[0] for p in slots_resp]
#     status = [[p[1], p[2]] for p in slots_resp]
#     slots = [[p[3], p[4]] for p in slots_resp]
#     return True, pincodes, status, slots

def get_pincodes(batch_no):
    try:
        slots_resp = Pincodes.query.filter_by(batch_no=batch_no).with_entities(Pincodes.pincode).all()
        if not slots_resp:
            return False, 'No pincodes yet'

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
    
    pincodes = [p[0] for p in slots_resp]
    return True, pincodes

# def get_slot_requests(pincode, age_group):
#     try:
#         slot_resp = Requests.query.filter_by(pincode=pincode).filter_by(age_group=age_group).filter_by(status=1).with_entities(Requests.sub_token, db.func.BIN_TO_UUID(Requests.request_id)).all()
#         if not slot_resp:
#             return False, 'No pincodes yet'

#     except Exception as err:
#         message = re.findall(r'\((.*?)\)', err.args[0])[-1]

#     sub_tokens = [p[0] for p in slot_resp]
#     request_ids = [p[1] for p in slot_resp]
#     return True, sub_tokens, request_ids

def get_pincode_requests(pincode):
    try:
        slot_resp = Requests.query.filter_by(pincode=pincode).filter_by(status=1).with_entities(Requests.sub_token, db.func.BIN_TO_UUID(Requests.request_id), Requests.age_group).all()
        
        if not slot_resp:
            return False, 'No pincodes yet'

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

    sub_tokens = [p[0] for p in slot_resp]
    request_ids = [p[1] for p in slot_resp]
    age_groups = [p[2] for p in slot_resp]
    return True, sub_tokens, request_ids, age_groups
    
def send_web_push(subscription_information, message_body):
    VAPID_PRIVATE_KEY = app.config['VAPID_PRIVATE_KEY']
    VAPID_CLAIMS = {
        "sub": "mailto:care@rebhu.com",
        #"aud": "https://getcovaxin.in",
        #"exp": int(time.time()) + (12 * 60 * 60)
    }
    return webpush(
        subscription_info=subscription_information,
        data=message_body,
        vapid_private_key=VAPID_PRIVATE_KEY,
        vapid_claims=VAPID_CLAIMS
    )
    


def update_n_push_send(request_id):
    try:
        n_push_send = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).with_entities(Requests.n_push_send).first()[0]
        n_push_send += 1
        n_push = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).update({'n_push_send': n_push_send}, synchronize_session=False)
        db.session.commit()
        
    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return False, message
    return True, "Count updated"


def send_push_notification(sub_tokens, request_ids, pincode, ag, log_info):
    max_num = len(sub_tokens)
    
    for i, sub_token in enumerate(sub_tokens):
        should_put = False
        if random.random() * max_num < 15:
            should_put = True
        
        # print("attending request no. ", i)
        payload = json.dumps({
            "request_id": request_ids[i],
            "pincode": pincode,
            "age_group": ag[i],
            "date": (dt.datetime.utcnow()+dt.timedelta(hours=5, minutes=30)).strftime('%d-%m-%Y'),
            "should_put": should_put
        })

        try:
            send_web_push(sub_token, payload)

        except WebPushException as ex:
            # log error
            log_info['request_id'] = request_ids[i]
            log_info['pincode'] = pincode
            log_info['message'] = ex
            log_push_web_error(log_info)

        
        resp = update_n_push_send(request_ids[i])
        if not resp[0]:
            print(resp)

# def add_slot_update(pincode, pincode_id, plus18_slots, plus45_slots):
#     try:
#         data = SlotRequests(
#             pincode_id = db.func.UUID_TO_BIN(pincode_id),
#             pincode = pincode,
#             plus18_slots = plus18_slots,
#             plus45_slots = plus45_slots,
#             time_stamp = dt.datetime.now()
#         )
#         db.session.add(data)
#         db.session.commit()
#     except Exception as err:
#         message = re.findall(r'\((.*?)\)', err.args[0])[-1]
#         return False, message
#     return True, "Added"