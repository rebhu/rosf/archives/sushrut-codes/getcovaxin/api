from .. import db

class Requests(db.Model):
    """DB model for Requests"""

    __tablename__ = "requests"

    id = db.Column(db.Integer, autoincrement=True, nullable=False, unique=True)
    request_id = db.Column(db.String(16), primary_key=True, nullable=False)
    pincode = db.Column(db.Integer, nullable=False)
    age_group = db.Column(db.Integer, nullable=False)
    ip_address = db.Column(db.String(20), nullable=True)
    latitude = db.Column(db.Float(20), nullable=True)
    longitude = db.Column(db.Float(20), nullable=True)
    status = db.Column(db.Boolean, nullable=False)
    sub_token = db.Column(db.JSON, nullable=True)
    n_push_received = db.Column(db.Integer, default=0, nullable=False)
    n_push_send = db.Column(db.Integer, default=0, nullable=False)
    os = db.Column(db.String(20), nullable=True)
    browser = db.Column(db.String(20), nullable=True)
    created_at = db.Column(db.DateTime, nullable=False)

class Pincodes(db.Model):
    """DB model for pincode slot availability"""

    __tablename__ = "pincodes"

    id = db.Column(db.Integer, autoincrement=True, nullable=False, unique=True)
    pincode_id = db.Column(db.String(16), primary_key=True, nullable=False)
    pincode = db.Column(db.Integer, nullable=False, unique=True)
    # plus18 = db.Column(db.JSON, nullable=True)
    # plus18_status = db.Column(db.Boolean, nullable=False)
    # plus45 = db.Column(db.JSON, nullable=True)
    # plus45_status = db.Column(db.Boolean, nullable=False)
    batch_no = db.Column(db.Integer, nullable=False)

class SlotRequests(db.Model):
    """DB Model for requests"""

    __tablename__ = "slot_requests"
    id = db.Column(db.Integer, autoincrement=True, nullable=False, primary_key=True)
    pincode_id = db.Column(db.String(16), nullable=False)
    pincode = db.Column(db.Integer, nullable=False, unique=True)
    plus18_slots = db.Column(db.Integer, nullable=False)
    plus45_slots = db.Column(db.Integer, nullable=False)
    time_stamp = db.Column(db.DateTime, nullable=False)
    