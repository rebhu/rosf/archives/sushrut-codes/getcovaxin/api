from ..model.getcovaxin_model import Requests, Pincodes
from ..support.slot_pub_support import get_pincodes, get_pincode_requests, send_push_notification
from ..support.request_support import number_of_pincodes
from ..util.error_responses import *
from app.main import db, sched
from flask import current_app as app

import json
import datetime as dt
import os

# ----------- SCHEDULED SERVICE FOR SLOT FETCHING ---------------
# @sched.task('interval', id='slot_get', minutes=int(os.getenv('SLOT_MINUTES')), jitter=int(os.getenv('SLOT_JITTER')), misfire_grace_time=900)
# def update_slots():
#     with sched.app.app_context():
#         current_batch_no = app.config['BATCH_NO_SLOT']
#         print('                             Schedule run for batch: ', current_batch_no)
#         # scan database for pincodes for their batch
#         pincodes = get_pincodes_and_status(current_batch_no)
#         if not pincodes[0]:
#             return False, ''
        
#         for i, pincode in enumerate(pincodes[1]):
#             pincode_id = pincodes[2][i]
#             slots_resp = get_pincode_slots(pincode=pincode)
#             if not slots_resp[0]:
#                 continue
            
#             slots18, slots45 = slots_resp[1], slots_resp[2]
        
#             # update slots data in database ++++++++++++
#             slot_status = {
#                 "plus18": json.dumps(slots18),
#                 "plus45": json.dumps(slots45),
#             }
            
#             try:
#                 update_resp = Pincodes.query.filter_by(pincode=pincode).update(slot_status)
#                 db.session.commit()

#             except Exception as err:
#                 message = re.findall(r'\((.*?)\)', err.args[0])[-1]

#                 return False, message
            
#             # update Slot Request data as well +++++++++
#             add_slot_resp = add_slot_update(pincode, pincode_id, slots18['slots'], slots45['slots'])

#         # update batch number
#         n_pincodes = number_of_pincodes()[1]
#         max_batch_no = (n_pincodes//50) + 1
#         if current_batch_no < max_batch_no:
#             app.config['BATCH_NO_SLOT'] = current_batch_no + 1
#         else:
#             app.config['BATCH_NO_SLOT'] = 1
            
        

# ----------- SCHEDULED SERVICE FOR SLOT PUBLISHING ---------------
# @sched.task('interval', id='slot_push', seconds=10, jitter=2, misfire_grace_time=900)
# @sched.task('interval', id='slot_push', seconds=int(os.getenv('PUSH_MINUTES')), jitter=int(os.getenv('PUSH_JITTER')), misfire_grace_time=900)
# def push_slots():
#     with sched.app.app_context():
#         # ---------------------------------------------------------------------
#         # open logfile if exists
#         # get current date
#         date_today = str(dt.datetime.now().date())
#         log_file_name = './logs/push_%s.logs' %(date_today)
#         # check if log file with same date exist
#         if not os.path.isfile(log_file_name):
#             log_file = open(log_file_name, 'w')
#         else:
#             log_file = open(log_file_name, 'a')

#         current_batch_no = app.config['BATCH_NO_PUSH']
#         log_info = {
#             'file_name': log_file,
#             'batch': current_batch_no
#         }
#         # ---------------------------------------------------------------------

#         print('                             Publishing run for batch: ', current_batch_no)
#         resp, pincodes, status, slots = get_slots(current_batch_no)
        
#         if not resp:
#             return False, ''

#         for i, pincode in enumerate(pincodes):
#             # publish the slots to pincodes ++++++++++++
#             st = status[i]
#             slot18, slot45 = {'slots': 0}, {'slots': 0}
            
#             if slots[i][0]:
#                 slot18 = slots[i][0]

#             if slots[i][1]: 
#                 slot45 = slots[i][1]
#             print("pincode: ", pincode)
#             if st[0] and slot18['slots']:
#                 print("Age group 18...")
#                 # publish for 18+
#                 # get all requests for these pincodes and age group
#                 requests_sub_tokens = get_slot_requests(pincode, age_group=18)
#                 if not requests_sub_tokens[0]:
#                     continue

#                 # push notifications for these sub_tokens and update n_push_send
#                 send_push_notification(sub_tokens=requests_sub_tokens[1], request_ids=requests_sub_tokens[2], data=slot18, pincode=pincode, ag=18, log_info=log_info)

#             if st[1] and slot45['slots']:
#                 print("Age group 45...")
#                 # publish for 45+
#                 requests_sub_tokens = get_slot_requests(pincode, age_group=45)
#                 if not requests_sub_tokens[0]:
#                     continue
#                 # push notifications for these sub_tokens
#                 send_push_notification(sub_tokens=requests_sub_tokens[1], request_ids=requests_sub_tokens[2], data=slot45, pincode=pincode, ag=45, log_info=log_info)
                
#         # update batch number
#         n_pincodes = number_of_pincodes()[1]
#         max_batch_no = (n_pincodes//50) + 1
#         if current_batch_no < max_batch_no:
#             app.config['BATCH_NO_PUSH'] = current_batch_no + 1
#         else:
#             app.config['BATCH_NO_PUSH'] = 1
        
#         log_file.close()
            
# ----------- SCHEDULED SERVICE FOR SLOT PUBLISHING ---------------
# @sched.task('interval', id='slot_push', seconds=20, jitter=2, misfire_grace_time=900)
@sched.task('interval', id='slot_push', minutes=int(os.getenv('PUSH_MINUTES')), jitter=int(os.getenv('PUSH_JITTER')), misfire_grace_time=900)
def push_slots():
    with sched.app.app_context():
        current_batch_no = app.config['BATCH_NO_PUSH']
        # ---------------------------------------------------------------------
        # open logfile if exists
        # get current date
        date_today = str(dt.datetime.now().date())
        log_file_name = './logs/push_%s.logs' %(date_today)
        
        #check if log directory exists
        if not os.path.isdir('./logs'):
            os.mkdir('./logs')

        # check if log file with same date exist
        if not os.path.isfile(log_file_name):
            log_file = open(log_file_name, 'w')
        else:
            log_file = open(log_file_name, 'a')

        
        log_info = {
            'file_name': log_file,
            'batch': current_batch_no
        }
        # ---------------------------------------------------------------------

        print('\n                             Publishing run for batch: \n', current_batch_no)
        resp, pincodes = get_pincodes(current_batch_no)
        
        if not resp:
            return False, ''

        for i, pincode in enumerate(pincodes):
            # publish the slots to pincodes ++++++++++++
            
            # print("pincode: ", pincode)
            
            # get all requests for these pincodes and age group
            pincode_requests = get_pincode_requests(pincode)
            if not pincode_requests[0]:
                continue

            # push notifications for these sub_tokens and update n_push_send
            send_push_notification(sub_tokens=pincode_requests[1], request_ids=pincode_requests[2], pincode=pincode, ag=pincode_requests[3], log_info=log_info)
            
        # update batch number
        n_pincodes = number_of_pincodes()[1]
        
        max_batch_no = (n_pincodes//50) + 1
        
        if current_batch_no < max_batch_no:
            app.config['BATCH_NO_PUSH'] = current_batch_no + 1
        else:
            app.config['BATCH_NO_PUSH'] = 1
        
        log_file.close()
            