import datetime as dt
import re

from ..model.getcovaxin_model import SlotRequests
from ..util.validation_schema import validate_data, escape_char
from ..support.pincode_support import get_pincode_id
from ..util.error_responses import *
from app.main import db

# ----------------------------------------------------------
#               ADD PINCODE SLOTS TO SLOT-REQUESTS
# ----------------------------------------------------------
def register_pincode_slots(data):
    # get number of slots
    slot18 = data['slots']['slot18']['dose1'] + data['slots']['slot18']['dose2']
    slot45 = data['slots']['slot45']['dose1'] + data['slots']['slot45']['dose2']

    # get pincode_id
    pincode_id = get_pincode_id(data['pincode'])
    if not pincode_id[0]:
        return error_response_403(pincode_id[1])

    # save data in slotRequests
    try:
        slot_data = SlotRequests(
            pincode_id = db.func.UUID_TO_BIN(pincode_id[1]),
            pincode = data['pincode'],
            plus18_slots = slot18,
            plus45_slots = slot45,
            time_stamp = dt.datetime.utcnow()
        )

        db.session.add(slot_data)
        db.session.commit()

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return error_response_403(message)


    return data, 200

