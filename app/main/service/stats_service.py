import datetime as dt
import re
import json
from flask import current_app as app

from ..model.getcovaxin_model import Requests, Pincodes
from .request_service import number_of_pincodes
from ..util.error_responses import *
from app.main import db


# ---------------------------------------------------------------------
#                           GET REQUESTS STATS
# ---------------------------------------------------------------------
def get_requests_stats():
    # get total number of requests, null requests, total push notifications
    try:
        total_requests = Requests.query.count()
        null_requests = Requests.query.filter_by(status=0).count()
        n_push_received = Requests.query.with_entities(db.func.sum(Requests.n_push_received)).scalar()
        n_push_send = Requests.query.with_entities(db.func.sum(Requests.n_push_send)).scalar()
        n_plus45 = Requests.query.filter_by(age_group=45).count()
        n_plus18 = Requests.query.filter_by(age_group=18).count()
        null_iphone_safari = Requests.query.filter_by(status=0).filter_by(os='iphone').filter_by(browser='safari').count()
        null_macos_firefox = Requests.query.filter_by(status=0).filter_by(os='macos').filter_by(browser='firefox').count()
        null_macos_chrome = Requests.query.filter_by(status=0).filter_by(os='macos').filter_by(browser='chrome').count()
        null_macos_safari = Requests.query.filter_by(status=0).filter_by(os='macos').filter_by(browser='safari').count()
        null_windows_firefox = Requests.query.filter_by(status=0).filter_by(os='windows').filter_by(browser='firefox').count()
        null_windows_chrome = Requests.query.filter_by(status=0).filter_by(os='windows').filter_by(browser='chrome').count()
        null_linux_firefox = Requests.query.filter_by(status=0).filter_by(os='linux').filter_by(browser='firefox').count()
        null_linux_chrome = Requests.query.filter_by(status=0).filter_by(os='linux').filter_by(browser='chrome').count()
        null_android_firefox = Requests.query.filter_by(status=0).filter_by(os='android').filter_by(browser='firefox').count()
        null_android_chrome = Requests.query.filter_by(status=0).filter_by(os='android').filter_by(browser='chrome').count()

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return error_response_403(message)

    bounce_rate = round(null_requests/total_requests, 3)
    
    response_object = {
        'total_requests': total_requests,
        'null_requests': {
            'total': null_requests,
            'iphone': {
                'safari': null_iphone_safari
            },
            'macos': {
                'firefox': null_macos_firefox,
                'chrome': null_macos_chrome,
                'safari': null_macos_safari
            },
            'windows': {
                'firefox': null_windows_firefox,
                'chrome': null_windows_chrome
            },
            'linux': {
                'firefox': null_linux_firefox,
                'chrome': null_linux_chrome
            },
            'android': {
                'firefox': null_android_firefox,
                'chrome': null_android_chrome
            }
        },
        'bounce_rate': bounce_rate,
        'number_of_push_received': int(n_push_received),
        'number_of_push_send': int(n_push_send),
        'number_of_plus45': n_plus45,
        'number_of_plus18': n_plus18
    }
    
    return response_object, 200


# ---------------------------------------------------------------------
#                           GET PINCODES STATS
# ---------------------------------------------------------------------
def get_pincode_stats():
    # batch numbers
    running_batch_push = app.config['BATCH_NO_PUSH']
    n_pincodes = number_of_pincodes()[1]

    # get total number of batches
    try:
        total_batch = Pincodes.query.with_entities(db.func.max(Pincodes.batch_no)).scalar()

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        return error_response_403(message)

    response_object = {
        'total_pincodes': n_pincodes,
        'total_batch': int(total_batch),
        'running_batch_push': running_batch_push
    }
    
    return response_object, 200