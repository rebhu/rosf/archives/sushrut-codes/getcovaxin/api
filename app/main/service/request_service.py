# service module for registration related operations

import uuid
import datetime as dt
import re
import json

from ..model.getcovaxin_model import Requests, Pincodes
from ..util.validation_schema import validate_data, escape_char
from ..support.request_support import get_lat_lon_from_ip, is_pincode_registered, update_pincode_status, verify_request_id, number_of_pincodes, update_n_push_received, get_pincode_age
from ..support.slot_pub_support import send_web_push
from ..util.error_responses import *
from app.main import db

# ---------------------------------------------------------------------
#                           REGISTRATIONS HANDLING
# ---------------------------------------------------------------------
def register_pincode(data, user_agent):

    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])

    
    # if validated then register --------------------------------------
    request_id = str(uuid.uuid4())

    lat, lon = get_lat_lon_from_ip(user_agent['ip_addr'])
    
    try:
        request_data = Requests(
                    request_id = db.func.UUID_TO_BIN(request_id),
                    pincode = data['pincode'],
                    age_group = data['age_group'],
                    ip_address = user_agent['ip_addr'],
                    created_at = dt.datetime.utcnow(),
                    latitude = lat,
                    longitude = lon,
                    os = user_agent['os'],
                    browser = user_agent['browser'],
                    status = 0
                    )
        
        save_changes(request_data)
        data['request_id'] = request_id
        
    except Exception as err:
        
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]
        
        return error_response_403(message)

    # =========================================================
    # register the pincode in pincode table as well
    # run query to check pincode
    # =========================================================
    is_pincode = is_pincode_registered(data['pincode'])
    n_pincodes = number_of_pincodes()[1]

    # get batch number for every 50th entry
    batch_no = (n_pincodes//50) + 1     

    if is_pincode[0]:
        return data, 201
        
    
    # otherwise register pincode
    pincode_id = str(uuid.uuid4())
    
    try: 
        pincode_data = Pincodes(
                pincode_id = db.func.UUID_TO_BIN(pincode_id),
                pincode = data['pincode'],
                batch_no = batch_no
        )

        save_changes(pincode_data)
        data['pincode_id'] = pincode_id

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return error_response_403(message)
    
    return data, 201

# ---------------------------------------------------------------------
#                           REQUEST ID VERIFICATION
# ---------------------------------------------------------------------
def request_id_verification(data):
    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])

    request_resp = verify_request_id(data['request_id'])
    if not request_resp[0]:
        return error_response_403(request_resp[1])
    
    return request_resp[1], 200

# ---------------------------------------------------------------------
#                           STORE SUBSCRIPTION TOKEN
# ---------------------------------------------------------------------
def store_sub_token(data):
    sub_token = {
        "sub_token": json.loads(data['sub_token']),
        "status": 1
    }
    request_id = data['request_id']

    try:
        update_resp = Requests.query.filter_by(request_id=db.func.UUID_TO_BIN(request_id)).update(sub_token, synchronize_session='fetch')
        db.session.commit()

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message

    return data, 200

# ---------------------------------------------------------------------
#                           PUSH NOTIFICATIONS
# ---------------------------------------------------------------------
def push_notification(data):
    sub_token = json.loads(data['sub_token'])

    # get request data
    pincode_resp = get_pincode_age(data['request_id'])
    if not pincode_resp[0]:
        return error_response_403(pincode_resp[1])

    payload = {
        "text": data['text'],
        "request_id": data['request_id'],
        "pincode": pincode_resp[1],
        "age_group": pincode_resp[2],
        "date": dt.datetime.now().strftime('%d-%m-%Y')
    }
    
    # sent push notification
    try:
        send_web_push(sub_token, json.dumps(payload))
    
    except Exception as err:
        return error_response_403(err)
    
    return data, 200

def update_n_push_count(data):
    # then update n_push in requests
    n_push_resp = update_n_push_received(data['request_id'])
    if not n_push_resp[0]:
        return error_response_403(n_push_resp[1])

    return data, 200

# support functions
def save_changes(data):
    db.session.add(data)
    db.session.commit()